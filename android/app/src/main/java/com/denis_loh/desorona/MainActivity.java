package com.denis_loh.desorona;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.MediaRouteActionProvider;
import android.support.v7.media.MediaRouteSelector;
import android.support.v7.media.MediaRouter;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.android.gms.cast.ApplicationMetadata;
import com.google.android.gms.cast.Cast;
import com.google.android.gms.cast.CastDevice;
import com.google.android.gms.cast.CastMediaControlIntent;
import com.google.android.gms.cast.LaunchOptions;
import com.google.android.gms.cast.games.GameManagerClient;
import com.google.android.gms.cast.games.GameManagerState;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    private MediaRouter mMediaRouter;
    private MediaRouteSelector mMediaRouteSelector;
    private CastDevice mSelectedDevice;
    private MediaRouterCallback mMediaRouterCallback = new MediaRouterCallback();
    private ConnectionCallbacks mConnectionCallbacks = new ConnectionCallbacks();
    private ConnectionFailedListener mConnectionFailedListener = new ConnectionFailedListener();
    private CastResultCallback mCastResultCallback = new CastResultCallback();
    private GoogleApiClient mApiClient;
    private boolean mApplicationStarted;
    private String mSessionId;
    private CastListener mCastListener = new CastListener();
    private boolean mWaitingForReconnect;
    private GameManagerClient mGameManagerClient;

    private class MediaRouterCallback extends MediaRouter.Callback {

        @Override
        public void onRouteSelected(MediaRouter router, MediaRouter.RouteInfo info) {
            mSelectedDevice = CastDevice.getFromBundle(info.getExtras());

            launchReceiver();
        }

        @Override
        public void onRouteUnselected(MediaRouter router, MediaRouter.RouteInfo info) {
            teardown();
            mSelectedDevice = null;
        }
    }

    private class ConnectionCallbacks implements GoogleApiClient.ConnectionCallbacks {

        @Override
        public void onConnected(Bundle bundle) {

            if (mApiClient == null) {
                return;
            }

            try {
                if (mWaitingForReconnect) {
                    mWaitingForReconnect = false;

                    if (bundle != null && bundle.getBoolean(Cast.EXTRA_APP_NO_LONGER_RUNNING)) {
                        teardown();
                    }
                } else {
                    LaunchOptions options = new LaunchOptions.Builder()
                            .setRelaunchIfRunning(false)
                            .build();
                    Cast.CastApi
                            .launchApplication(mApiClient, getString(R.string.google_cast_key), options)
                            .setResultCallback(mCastResultCallback);
                }
            } catch (Exception e) {
                Log.e(TAG, "Failed to launch application", e);
            }
        }

        @Override
        public void onConnectionSuspended(int i) {
            mWaitingForReconnect = true;
        }
    }

    private class ConnectionFailedListener implements GoogleApiClient.OnConnectionFailedListener {

        @Override
        public void onConnectionFailed(ConnectionResult connectionResult) {
            teardown();
        }
    }

    private class CastResultCallback implements ResultCallback<Cast.ApplicationConnectionResult> {
        @Override
        public void onResult(Cast.ApplicationConnectionResult applicationConnectionResult) {
            Status status = applicationConnectionResult.getStatus();
            if (status.isSuccess()) {
                ApplicationMetadata applicationMetadata =
                        applicationConnectionResult.getApplicationMetadata();
                String applicationStatus =
                        applicationConnectionResult.getApplicationStatus();

                mSessionId = applicationConnectionResult.getSessionId();
                Log.d(TAG, "Application: " + applicationMetadata.getName() + " " +
                            "(SID: " + mSessionId +
                            ", status=" + applicationStatus + ")");
                mApplicationStarted = true;

                GameManagerClient.getInstanceFor(mApiClient, mSessionId)
                        .setResultCallback(new GameManagerClientResultCallback());
            } else {
                teardown();
            }
        }
    }

    private class CastListener extends Cast.Listener {
        @Override
        public void onApplicationDisconnected(int statusCode) {
            teardown();
            super.onApplicationDisconnected(statusCode);
        }
    }

    private class GameManagerClientResultCallback
            implements ResultCallback<GameManagerClient.GameManagerInstanceResult> {
        @Override
        public void onResult(
                GameManagerClient.GameManagerInstanceResult gameManagerInstanceResult) {
            mGameManagerClient = gameManagerInstanceResult.getGameManagerClient();
            onGameConnected();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Message sent", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        mMediaRouter = MediaRouter.getInstance(getApplicationContext());
        mMediaRouteSelector = new MediaRouteSelector.Builder()
                .addControlCategory(
                        CastMediaControlIntent.categoryForCast(getString(R.string.google_cast_key)))
                .build();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mMediaRouter.addCallback(mMediaRouteSelector, mMediaRouterCallback,
                MediaRouter.CALLBACK_FLAG_REQUEST_DISCOVERY);
    }

    @Override
    protected void onStop() {
        mMediaRouter.removeCallback(mMediaRouterCallback);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        teardown();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        MenuItem mediaRouteMenuItem = menu.findItem(R.id.media_route_menu_item);
        MediaRouteActionProvider mediaRouteActionProvider =
                (MediaRouteActionProvider) MenuItemCompat.getActionProvider(mediaRouteMenuItem);
        mediaRouteActionProvider.setRouteSelector(mMediaRouteSelector);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void launchReceiver() {
        try {
            Cast.CastOptions apiOptions =
                    Cast.CastOptions.builder(mSelectedDevice, mCastListener)
                            .setVerboseLoggingEnabled(true)
                            .build();

            mApiClient = new GoogleApiClient.Builder(this)
                    .addApi(Cast.API, apiOptions)
                    .addConnectionCallbacks(mConnectionCallbacks)
                    .addOnConnectionFailedListener(mConnectionFailedListener)
                    .build();

            mApiClient.connect();
        } catch (Exception e) {
            Log.e(TAG, "Failed to launch receiver.", e);
        }
    }

    private void teardown() {
        Log.d(TAG, "Teardown Google cast");
        if (mApiClient != null) {
            if (mApplicationStarted) {
                if (mApiClient.isConnected() || mApiClient.isConnecting()) {
                    try {
                        Cast.CastApi.stopApplication(mApiClient, mSessionId);
                        mGameManagerClient.dispose();
                    } catch (Exception e) {
                        Log.e(TAG, "Failed to stop application", e);
                    }
                    mApiClient.disconnect();
                }
                mApplicationStarted = false;
            }
            mApiClient = null;
        }
        mSelectedDevice = null;
        mSessionId = null;
    }

    private void onGameConnected() {
        TextView tv = (TextView) findViewById(R.id.status);

        GameManagerState state = mGameManagerClient.getCurrentState();

        String status = state.getApplicationName() + "\n" +
                "Status: " + state.getGameStatusText() + "\n" +
                "Gameplay state: " + state.getGameplayState() + "\n";

        tv.setText(status);
    }

}
